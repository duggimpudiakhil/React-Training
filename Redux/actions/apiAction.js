import axios from "axios";


export const fetchData = () => {
    return async (dispatch, getState) => {
        dispatch({
            type:"FETCH_DATA_BEGIN"
        })
        try {
            const response = await axios.get('https://api.publicapis.org/entries');
            //const response=await axios.get('https://www.programmableweb.com/category/music/api');

            dispatch({
                type: "FETCH_DATA_SUCCESS",
                data: response.data
            })

        }
        catch (error) {
            dispatch({
                type:"FETCH_DATA_ERROR",
                data:error
            })
            console.log(error)
        }
    }
}

export const getData=(item)=>({
type:"GET_DATA",
data:item
})
