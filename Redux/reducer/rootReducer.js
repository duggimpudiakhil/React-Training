import {combineReducers} from 'redux'
import {persistReducer} from 'redux-persist'

import AsyncStorage from '@react-native-community/async-storage';

import apiReducer from './childreducer/apiReducer'
import loginReducer from './childreducer/loginReducer'
import employeeReducer from './childreducer/employeeReducer'

const persistConfig={
  key:'root',
  storage : AsyncStorage,
  whitelist:['api','login'] 
}
const rootReducer= combineReducers({
  api: apiReducer,
  login: loginReducer,
  employee:employeeReducer
})

export default persistReducer(persistConfig,rootReducer)
