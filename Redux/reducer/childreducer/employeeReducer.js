import { useNavigation } from '@react-navigation/native';

const initialEmployeeState={
    employeeData:[
        {
            "name": "Miyah Myles",
            "email": "miyah.myles@gmail.com",
            "position": "Data Entry Clerk",
            "photo": "https:\/\/images.unsplash.com\/photo-1494790108377-be9c29b29330?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=707b9c33066bf8808c934c8ab394dff6"
        },
        {
            "name": "June Cha",
            "email": "june.cha@gmail.com",
            "position": "Sales Manager",
            "photo": "https://i.ndtvimg.com/i/2017-08/indian-food_650x400_81501923865.jpg"
        },
        {
            "name": "Iida Niskanen",
            "email": "iida.niskanen@gmail.com",
            "position": "Sales Manager",
            "photo": "https:\/\/randomuser.me\/api\/portraits\/women\/68.jpg"
        },
        {
            "name": "Renee Sims",
            "email": "renee.sims@gmail.com",
            "position": "Medical Assistant",
            "photo": "https:\/\/randomuser.me\/api\/portraits\/women\/65.jpg"
        },
        {
            "name": "Jonathan Nu\u00f1ez",
            "email": "jonathan.nu\u00f1ez@gmail.com",
            "position": "Clerical",
            "photo": "https:\/\/randomuser.me\/api\/portraits\/men\/43.jpg"
        },
        {
            "name": "Sasha Ho",
            "email": "sasha.ho@gmail.com",
            "position": "Administrative Assistant",
            "photo": "https:\/\/images.pexels.com\/photos\/415829\/pexels-photo-415829.jpeg?h=350&auto=compress&cs=tinysrgb"
        },
        {
            "name": "Abdullah Hadley",
            "email": "abdullah.hadley@gmail.com",
            "position": "Marketing",
            "photo": "https:\/\/images.unsplash.com\/photo-1507003211169-0a1dd7228f2d?ixlib=rb-0.3.5&q=80&fm=jpg&crop=entropy&cs=tinysrgb&w=200&fit=max&s=a72ca28288878f8404a795f39642a46f"
        },
        {
            "name": "Thomas Stock",
            "email": "thomas.stock@gmail.com",
            "position": "Product Designer",
            "photo": "https:\/\/tinyfac.es\/data\/avatars\/B0298C36-9751-48EF-BE15-80FB9CD11143-500w.jpeg"
        },
        {
            "name": "Veeti Seppanen",
            "email": "veeti.seppanen@gmail.com",
            "position": "Product Designer",
            "photo": "https:\/\/randomuser.me\/api\/portraits\/men\/97.jpg"
        },
        {
            "name": "Bonnie Riley",
            "email": "bonnie.riley@gmail.com",
            "position": "Marketing",
            "photo": "https:\/\/randomuser.me\/api\/portraits\/women\/26.jpg"
        }
    ],
    foodData:[
        {
            "name": "Plain Rice with naans",
            "email": "miyah.myles@gmail.com",
       
            "position": "Cooked rice refers to rice that has been cooked either by steaming or boiling. The terms steamed rice or boiled rice are also commonly used. Any variant of Asian rice (both Indica and Japonica varieties).",
            "photo": "https://i.ndtvimg.com/i/2017-08/indian-food_650x400_81501923865.jpg"
        },
        {
            "name": "Snacks",
            "email": "june.cha@gmail.com",
            
            "position":"Roti with Chicken Strips with green chutney and mixed fruit milkshakes",
            "photo": "https://b.zmtcdn.com/data/pictures/7/18273537/5efa1fe0564eb5cad0f917dc74728537_featured_v2.jpg?fit=around|771.75:416.25&crop=771.75:416.25;*,*"
        },
        {
            "name": "Roti with Panner Butter Masala",
            "email": "iida.niskanen@gmail.com",
           
            "position":"Plain Rotis(3) with Panner Butter masala curry and green chutney and strawberries with green vegies",
            "photo": "https://economictimes.indiatimes.com/thumb/msid-70143550,width-1200,height-900,resizemode-4,imgsize-1479961/zomato-indias-initial-tweet-was-liked-by-over-19000-users-and-re-tweeted-nearly-4000-times-.jpg?from=mdr"
        },
        {
            "name": "Veg Pizza with extra cheese",
            "email": "renee.sims@gmail.com",
            
            "position":"Veg Pizza with all vegetables mixed and extra cheese with tomato ketchup",
            "photo": "https://i.pinimg.com/originals/d9/42/87/d94287f4c1dfb9920c9f3596ab2745b1.jpg"
        },
        {
            "name": "Chicken DrumSticks",
            "email": "jonathan.nu\u00f1ez@gmail.com",
            
            "position":"Chicken Drum Stickes deep fried and extra crispy and potato fries",
            "photo": "https://hips.hearstapps.com/hmg-prod.s3.amazonaws.com/images/delish-190808-baked-drumsticks-0185-landscape-pf-1567089281.jpg"
        },
        {
            "name": "Chicken Dum Biryani",
            "email": "sasha.ho@gmail.com",
     
            "position":"Biryani is a mixed rice dish originating among the Muslims of the Indian subcontinent. It is made with Indian spices, rice, and meat (chicken).",
            "photo": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcThugz_Rlotkwx8YRRZnDsVjRjUSZxcmEHtBB0_ujOn_59--Yvt3WvvnfjODRoUM1TSOh0&usqp=CAU"
        },
        {
            "name": "Green Veg Thali",
            "email": "abdullah.hadley@gmail.com",
            
            "position":"Green veg thali with roti(2), plain rice and dal with 2 curries",
            "photo": "https://encrypted-tbn0.gstatic.com/images?q=tbn:ANd9GcRwiJKdmEINt2uj2itYK4ODazvoAxaSFh1K_w&usqp=CAU"
        },
        {
            "name": "Chicken 65",
            "email": "thomas.stock@gmail.com",
            
            "position":" Chicken 65 pieces deeply barbequed and with extra ketchup and onions",
            "photo": "https://upload.wikimedia.org/wikipedia/commons/5/5d/Chicken_65_%28Dish%29.jpg"
        },
        {
            "name": "Burger with french fries",
            "email": "veeti.seppanen@gmail.com",
          
            "position":"Burger with 2 chicken rings and some french fries with coke",
            "photo": "https://media.istockphoto.com/photos/hamburger-with-cheese-and-french-fries-picture-id1188412964?k=6&m=1188412964&s=612x612&w=0&h=860S0kVoWTA60T4wakI8HLv-Y3fFw9jOKLSB4quEkIE="
        },
        {
            "name": "Pav-Bhaji",
            "email": "bonnie.riley@gmail.com",
            
            "position":"Pav-Bhaji with 2 breads and some tomoto curry and boiled egg",
            "photo": "https://asset20.ckassets.com/blog/wp-content/uploads/sites/5/2019/12/Pav-Bhaji.jpg" 
          }
    ],
    detailsData:'',
}

const employeeReducer= (state=initialEmployeeState,action) => {
 
    switch(action.type){
        case "EMPLOYEE_DATA":
          return{
              ...state,
              detailsData:action.data
          }
          case "FOOD_DATA":
            return{
                ...state,
                detailsData:action.data
            }
    }
    return state
}
export default employeeReducer;