import {DO_CHANGEU,DO_CHANGE,DO_RESET} from 'akhilproject/Redux/actions/loginaction'

const initialState = {
    uname: "",
    pwd: "",
    photo: "https://images.unsplash.com/photo-1511367461989-f85a21fda167?ixid=MnwxMjA3fDB8MHxzZWFyY2h8Mnx8cHJvZmlsZXxlbnwwfHwwfHw%3D&ixlib=rb-1.2.1&w=1000&q=80",
   
  }

 const loginReducer = (state=initialState,action) =>{
 
    switch(action.type){
    case "CHANGE_PIC":
      return{
        ...state,
        photo:action.data
      }
    case "DO_CHANGEU":
    
      return{
      ...state,
       uname: action.data
      }
    
    case "DO_CHANGE":
      
      return{
        ...state,
        pwd : action.data
       
      }
    
    case "DO_RESET":
     
     return{
       ...state,
        uname: "",
        pwd: ""
      }
   
   
    }  
    return state
   
    
  }
  export default loginReducer;