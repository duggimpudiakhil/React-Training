
const initialData = {
  isLoading: false,
  datalist: [],
  error:"",
  apidata:''
}

const apiReducer = (state = initialData, action) => {

  switch (action.type) {
    case 'FETCH_DATA_BEGIN':
      return {
        ...state,
        isLoading: true,
      }
    case 'FETCH_DATA_SUCCESS':
      return {
        ...state,
        isLoading: false,
        datalist:action.data,
      }

    case 'FETCH_DATA_ERROR':
      return {
        ...state,
        isLoading: false,
        error: action.data
      }
    case 'GET_DATA':
      return{
        ...state,
        apidata:action.data
      }
    // default:
    //   return{
    //     ...state
    //   }
  }
  return state

}
export default apiReducer;