import {createStore,applyMiddleware} from 'redux'

import persistReducer from './reducer/rootReducer'
import thunk from 'redux-thunk'

import {persistStore} from 'redux-persist'
 export const store=createStore(
     persistReducer,
     applyMiddleware(thunk)
     );
    
 export const persistor = persistStore(store)