
// import React, { Component } from 'react';
// import {
//   Text,
//   StyleSheet
// } from 'react-native';
// import MapView from 'react-native-maps';
// //import ClusteredMapView from 'react-native-maps-super-cluster';
// //import image from './images/flag-pink.png';

// export default class App extends Component {
//   render() {

//     const coordinates = [];

//     coordinates.push({
//       key: 0,
//       location: {
//         longitude: -70.23,
//         latitude: -33.23
//       }
//     });

//     for(let i = 1; i<100; i++) {

//       const location = {
//         longitude: coordinates[i-1].location.longitude + (Math.random() * (i%2 === 0 ? -1 : 1)),
//         latitude: coordinates[i-1].location.latitude + (Math.random() * (i%2 === 0 ? -1 : 1)),
//       };

//       coordinates.push({ key: i, location });

//     }

//     return (
//       <MapView
//         renderMarker={renderMarker}
//         initialRegion={{
//           longitude: -70.23,
//           latitude: -33.23,
//           latitudeDelta: 9.22,
//           longitudeDelta: 4.21,
//         }}
//         style={StyleSheet.absoluteFillObject}>

//         {/* { coordinates.map(({ key, location } ) => <MapView.Marker key={key} image={image} coordinate={location} />) } */}

//       </MapView>
//     );
//   }
// }

// function renderMarker({ location }) {
//   return (
//     <MapView.Marker
//       image={image}
//       coordinate={location}
//     >
//       <MapView.Callout>
//         <Text>BiG BiG Callout</Text>
//       </MapView.Callout>
//     </MapView.Marker>
//   );
// }
import {Provider} from 'react-redux'

import {MyStack} from './Components/Routes'
  
import {PersistGate} from 'redux-persist/integration/react'
import {store,persistor} from './Redux/store'
import React, {Component } from 'react';


export default class App extends Component{
  
  
  render(){
      return(
      <Provider store={store}>
        <PersistGate persistor={persistor}>
           <MyStack/>
        </PersistGate>
      </Provider>
     
    
      );
    }
  }



  


// import * as React from 'react';
// import { StyleSheet, Text, View, Button } from 'react-native';
// import { TouchableOpacity } from 'react-native-gesture-handler';
// import Animated from 'react-native-reanimated';
// import BottomSheet from 'reanimated-bottom-sheet';

// export default function App() {
//   const renderContent = () => (
//     <View
//       style={{
//         backgroundColor: 'white',
//         padding: 16,
//         height: 450,
//       }}
//     >
//       <Button
//       title={"Hello"}
//       onPress={()=>sheetRef.current.snapTo(1)}
//       />
//     </View>
//   );

//   const sheetRef = React.createRef();

//   return (
//     <>
//       <View
//         style={{
//           flex: 1,
//           backgroundColor: 'papayawhip',
//           alignItems: 'center',
//           justifyContent: 'center',
//         }}
//       >
//         <Button
//           title="Open Bottom Sheet"
//           onPress={() =>sheetRef.current.snapTo(0)}
//         />
//       </View>
//       <BottomSheet
//         ref={sheetRef}
//         snapPoints={[300,0]}
//         borderRadius={10}
//         renderContent={renderContent}
//         initialSnap={1}
//        enabledGestureInteraction={true}
//        enabledHeaderGestureInteraction={false} 
//        enabledContentGestureInteraction={true}
//       />
//     </>
//   );
// }

// import React, { Fragment, Component } from 'react';
// import ImagePicker from 'react-native-image-crop-picker';
// import Icon from 'react-native-vector-icons/FontAwesome'
// import {
//   SafeAreaView,
//   StyleSheet,
//   ScrollView,
//   View,
//   Text,
//   StatusBar,
//   Image,
//   Button,
//   Dimensions,
//   TouchableOpacity
// } from 'react-native';

// import {
//   Header,
//   LearnMoreLinks,
//   Colors,
//   DebugInstructions,
//   ReloadInstructions,
// } from 'react-native/Libraries/NewAppScreen';
// import BottomSheet from 'reanimated-bottom-sheet'
// import Animated from 'react-native-reanimated';

// export default class App extends Component {
//   constructor(props) {
//     super(props)
//     this.state = {
//       fileData: 'https://media.nature.com/lw800/magazine-assets/d41586-020-01430-5/d41586-020-01430-5_17977552.jpg',
//       ref: React.createRef(),
//       fall:new Animated.Value(1)
//     }
//   }

// launchCamera(){
//   ImagePicker.openCamera({
//     width: 300,
//     height: 400,
//     cropping: true,
//   }).then(image => {
//    this.setState({
//      fileData:image.path
//    }) 
//     console.log(image);
//   });
// }

// launchImageLibrary(){
//   ImagePicker.openPicker({
//     width: 300,
//     height: 400,
//     cropping: true
//   }).then(image => {
//     console.log(image);
//   });
// }
// listItems () {
//  console.log("HII AKH") 
//  return (
  
  
//   <View style={styles.btnParentSection}>
   
//     <Text>hii</Text>  
//     <TouchableOpacity onPress={this.launchCamera} style={styles.btnSection}  >
//       <Text style={styles.btnText}>Directly Launch Camera</Text>
//     </TouchableOpacity>

//     <TouchableOpacity onPress={this.launchImageLibrary} style={styles.btnSection}  >
//       <Text style={styles.btnText}>Directly Launch Image Library</Text>
//     </TouchableOpacity>       
    
//   </View>
//   );
// }
// renderHeader = () =>(
//   <View>
//     <Text>HII</Text>
//   </View>
// )
//   render() {
//     return (
//           <View style={styles.body}>
         
            
//             <Text style={{textAlign:'center',fontSize:20,paddingBottom:10}} >Pick Images from Camera & Gallery</Text>
//             <View style={styles.ImageSections}>
//               <View>
//                 <Text  style={{textAlign:'center'}}>Base 64 String</Text>
//               </View>
//             </View>
//             <View>
//               <TouchableOpacity
//               onPress={()=>this.state.ref.current.snapTo(0)} 
//               >
//                 <Image source={{uri:this.state.fileData}}
//                  style={styles.images}  
//                 />
//               </TouchableOpacity>
//             </View>
//             <BottomSheet 
//             ref={this.state.ref}
//             snapPoints={[300,0]}
//             renderContent={this.listItems}
//             renderHeader={this.renderHeader}
//             initialSnap={1}
//             callbackNode={this.state.fall}
//             enabledGestureInteraction={true}
//             />
            

//           </View>
      
    
//     );
//   }
// };

// const styles = StyleSheet.create({
//   scrollView: {
//     backgroundColor: Colors.lighter,
//   },

//   body: {
//     backgroundColor: Colors.white,
//     justifyContent: 'center',
//     borderColor: 'black',
//     borderWidth: 1,
//     height: Dimensions.get('screen').height - 20,
//     width: Dimensions.get('screen').width
//   },
//   ImageSections: {
//     display: 'flex',
//     flexDirection: 'row',
//     paddingHorizontal: 8,
//     paddingVertical: 8,
//     justifyContent: 'center'
//   },
//   images: {
//     width: 150,
//     height: 150,
//     borderColor: 'black',
//     borderWidth: 1,
//     marginHorizontal: 3
//   },
//   btnParentSection: {
//     alignItems: 'center',
//     marginTop:10
//   },
//   btnSection: {
//     width: 225,
//     height: 50,
//     backgroundColor: '#DCDCDC',
//     alignItems: 'center',
//     justifyContent: 'center',
//     borderRadius: 3,
//     marginBottom:10
//   },
//   btnText: {
//     textAlign: 'center',
//     color: 'gray',
//     fontSize: 14,
//     fontWeight:'bold'
//   }
// });
// // import axios from 'axios';



//   // import {
// //   Colors,
// //   DebugInstructions,
// //   Header,
// //   LearnMoreLinks,
// //   ReloadInstructions,
// // } from 'react-native/Libraries/NewAppScreen';

// // function Home({navigation}){
// //   //const navigation = useNavigation();
// //  const [uname,setUname]=useState('');
// //  const [pwd,setPwd]=useState('');
// //  var un="Admin"
// //  var pss="Admin"
// //     return(
    
    
// //       <View style={{flex:1,backgroundColor:"silver"}}>
// //         <View style={{flex:0.2,backgroundColor:"violet"}}>
// //         <Text style={{fontSize:40,justifyContent:"center",marginLeft:"20%",color:"blue",top:20}}>Login Page</Text>
// //         </View>
// //         <View
// //         style={{flex:0.5,backgroundColor:"pink"}}
// //         >
// //         <Text style={{fontSize:20,justifyContent:"center",marginLeft:"20%",color:"grey",top:"15%"}}>Username:</Text>
// //         <TextInput
// //         style={{fontSize:18,backgroundColor:"violet",marginLeft:"20%",marginRight:"20%",top:"20%"}}
// //         placeholder="Username"
// //         onChangeText={(text)=>{
// //           setUname({
// //           uname:text
// //           })
// //         }
// //       }
// //         >

// //         </TextInput>
// //         <Text style={{fontSize:20,justifyContent:"center",marginLeft:"20%",color:"grey",top:"25%"}}>Password:</Text>
// //         <TextInput
// //         style={{fontSize:18,justifyContent:"center",marginLeft:"20%",backgroundColor:"violet",marginRight:"20%",top:"30%"}}
// //         placeholder="Password"
// //         onChangeText={(text)=>{
// //          console.log(text)
// //           setPwd(
// //           text.target.value
// //           )
          
        
         
// //         }
// //       }
// //         />
// //         </View>
// //         <View
// //         style={{marginLeft:"35%",justifyContent:"center",marginRight:"40%",backgroundColor:"skyblue",height:40}}
// //         >
// //           <TouchableOpacity
// //            styles={{}}
           
// //            onPress={()=>{
// //          console.log(pwd[1])
// //            if(un=="Admin" && pss=="Admin"){
// //             // Alert.alert("You Pressed",this.state.uname)
// //           //console.log(this.state.uname)
           
// //           navigation.navigate("Login")
// //            }
// //           else{
// //             Alert.alert("Invalid Credentials")
           
// //           }
// //           }
// //         }
// //           >
// //             <Text style={{fontSize:20,marginLeft:"20%",marginRight:"20%"}}>Login</Text>
// //           </TouchableOpacity>
// //         </View>
// //       </View>
 
// //   );
// // }

// // function LoginPage(){
// // return(
// //   <Text>You Have Logged In Successfully</Text>
// // );
// // }
// // const Stack=createStackNavigator();
// //  const App=() => {
// //      return(
// //       <NavigationContainer>
// //          <Stack.Navigator>
// //              <Stack.Screen
// //              name="Home"
// //              component={Home}
// //              options={{headerShown:false}}          
// //              />
// //              <Stack.Screen
// //              name="Login"
// //              component={LoginPage}
// //              options={{title:"Details"}}
// //              />
// //              {/* <Stack.Screen name="Signup" component={Login2} options={{title:"Signup"}}/> */}
// //              {/* <Stack.Screen
// //              name="Login"
// //              component={login}
// //              options={{title:"Login"}}
// //              /> */}
// //          </Stack.Navigator>
// //       </NavigationContainer>
// //      );
      
// //   }
// //   export default App;

//   // constructor(props){
//   //   super(props)
//   //   this.state={
//   //     uname:"",
//   //     password:""
//   //   }

//   // }
  
//   // render(navigate){
//   //   return(
    
    
//   //     <View style={{flex:1,backgroundColor:"silver"}}>
//   //       <View style={{flex:0.2,backgroundColor:"violet",marginLeft:"10%",marginRight:"10%"}}>
//   //       <Text style={{fontSize:40,justifyContent:"center",marginLeft:"20%",color:"blue",top:20}}>Login Page</Text>
//   //       </View>
//   //       <View
//   //       style={{flex:0.8,backgroundColor:"pink"}}
//   //       >
//   //       <Text style={{fontSize:20,justifyContent:"center",marginLeft:"20%",color:"grey",top:"15%"}}>Username:</Text>
//   //       <TextInput
//   //       style={{fontSize:18,backgroundColor:"violet",marginLeft:"20%",marginRight:"20%",top:"20%"}}
//   //       placeholder="Username"
//   //       onChangeText={(text)=>{
//   //            this.state.uname=text;
//   //       }}
//   //       >

//   //       </TextInput>
//   //       <Text style={{fontSize:20,justifyContent:"center",marginLeft:"20%",color:"grey",top:"25%"}}>Password:</Text>
//   //       <TextInput
//   //       style={{fontSize:18,justifyContent:"center",marginLeft:"20%",backgroundColor:"violet",marginRight:"20%",top:"30%"}}
//   //       placeholder="Password"
//   //       />
//   //       </View>
//   //       <View
//   //       style={{marginLeft:"35%",justifyContent:"center",marginRight:"40%",backgroundColor:"red",height:40}}
//   //       >
//   //         <TouchableOpacity
//   //          styles={{}}
//   //          onPress={()=>{
//   //           // if(this.state.uname=="Admin"){
//   //           // Alert.alert("You Pressed",this.state.uname)
//   //         navigate.navigate("Login")
//   //          }
          
//   //         }
//   //         >
//   //           <Text style={{fontSize:20,marginLeft:"20%",marginRight:"20%"}}>Login</Text>
//   //         </TouchableOpacity>
//   //       </View>
//   //     </View>
//   //  // </NavigationContainer>
//   //   )
//   // }

//   // import HomeActivity from './Components/HomeActivity'
// // import GridMenu from './Components/Grid'
// // import FoodItems from './Components/FoodItems'
// // import MobileItems from './Components/MobileItems'
// // import Details from './Components/Details'
  

// // const Stack=createStackNavigator()
// //   const MyStack=()=>{
      
// //      return(
// //       <NavigationContainer>
// //          <Stack.Navigator>
// //              <Stack.Screen
// //              name="Login"
// //              component={HomeActivity}
// //              options={{headerShown:false}}
// //              />
// //              <Stack.Screen name="Grid" component={GridMenu} options={{title:"Menu"}}/>
// //              <Stack.Screen name="Food" component={FoodItems} options={{title:"Food List"}}/>
// //              <Stack.Screen name="Mobile" component={MobileItems} options={{title:"Mobiles"}}/>
// //              <Stack.Screen name="Details" component={Details} options={{title:"Details"}}/>
// //          </Stack.Navigator>
// //       </NavigationContainer>
// //      );
      
// //   }


// //import {NavigationContainer} from "@react-navigation/native"



// //import {createStackNavigator} from "@react-navigation/stack"

// //import { useState } from "react";
// //import { SafeAreaInsetsContext } from "react-native-safe-area-context";


// //import {createStore} from 'redux';



// //Redux Usage


// //   const initialState = {
// //     uname: "",
// //     pwd: ""
// //   }
// //   const reducer=(state=initialState,action)=>{
// //    // console.log(action.text)
   
// //     switch(action.type){
// //     case 'DO_CHANGEU':
      
// //       return{
// //       ...state,
// //        uname: action.text
// //       }
    
// //     case 'DO_CHANGE':
// //       return{
// //         ...state,
// //         pwd : action.val
       
// //       }
    
// //     case 'DO_RESET':
// //      // console.log(state.uname)
// //      return{
// //         uname: state.uname="",
// //         pwd:  state.pwd=""
// //       }
// //     }   
// //    return state
    
// //   }
// // const store=createStore(reducer);
// // // const Section = ({children, title}): Node => {
// // //   const isDarkMode = useColorScheme() === 'dark';
// // //   return (
// // //     <View style={styles.sectionContainer}>
// // //       <Text
// // //         style={[
// // //           styles.sectionTitle,
// // //           {
// // //             color: isDarkMode ? Colors.white : Colors.black,
// // //           },
// // //         ]}>
// // //         {title}
// // //       </Text>
// // //       <Text
// // //         style={[
// // //           styles.sectionDescription,
// // //           {
// // //             color: isDarkMode ? Colors.light : Colors.dark,
// // //           },
// // //         ]}>
// // //         {children}
// // //       </Text>
// // //     </View>
// // //   );
// // // };

// // // const App: () => Node = () => {
// // //   const isDarkMode = useColorScheme() === 'dark';

// // //   const backgroundStyle = {
// // //     backgroundColor: isDarkMode ? Colors.darker : Colors.lighter,
// // //   };

// // //   return (
// // //     <SafeAreaView style={backgroundStyle}>
// // //       <StatusBar barStyle={isDarkMode ? 'light-content' : 'dark-content'} />
// // //       <ScrollView
// // //         contentInsetAdjustmentBehavior="automatic"
// // //         style={backgroundStyle}>
// // //         <Header />
// // //         <View
// // //           style={{
// // //             backgroundColor: isDarkMode ? Colors.black : Colors.white,
// // //           }}>
// // //           <Section title="Step One">
// // //             Edit <Text style={styles.highlight}>App.js</Text> to change this
// // //             screen and then come back to see your edits.
// // //           </Section>
// // //           <Section title="See Your Changes">
// // //             <ReloadInstructions />
// // //           </Section>
// // //           <Section title="Debug">
// // //             <DebugInstructions />
// // //           </Section>
// // //           <Section title="Learn More">
// // //             Read the docs to discover what to do next:
// // //           </Section>
// // //           <LearnMoreLinks />
// // //         </View>
// // //       </ScrollView>
// // //     </SafeAreaView>
// // //   );
// // // };

// // const styles = StyleSheet.create({
// //   sectionContainer: {
// //     marginTop: 32,
// //     paddingHorizontal: 24,
// //   },
// //   sectionTitle: {
// //     fontSize: 24,
// //     fontWeight: '600',
// //   },
// //   sectionDescription: {
// //     marginTop: 8,
// //     fontSize: 18,
// //     fontWeight: '400',
// //   },
// //   highlight: {
// //     fontWeight: '700',
// //   },
// // });

// // //export default App;

// // React Native Axios – To Make HTTP API call in React Native
// // https://aboutreact.com/react-native-axios/

// // import React from 'react';
// // //import React in our code.
// // import {StyleSheet, View, TouchableOpacity, Text} from 'react-native';
// // //import all the components we are going to use.
// // import axios from 'axios';

// // const App = () => {
// //   const getDataUsingSimpleGetCall = () => {
// //     axios
// //       .get('https://jsonplaceholder.typicode.com/posts/1')
// //       .then(function (response) {
// //         // handle success
// //         alert(JSON.stringify(response.data));
// //       })
// //       .catch(function (error) {
// //         // handle error
// //         alert(error.message);
// //       })
// //       .finally(function () {
// //         // always executed
// //         alert('Finally called');
// //       });
// //   };

// //   const getDataUsingAsyncAwaitGetCall = async () => {
// //     try {
// //       const response = await axios.get(
// //         'https://jsonplaceholder.typicode.com/posts/1',
// //       );
// //       alert(response.data);
// //     } catch (error) {
// //       // handle error
// //       alert(error.message);
// //     }
// //   };

// //   const postDataUsingSimplePostCall = () => {
// //     axios
// //       .post('https://jsonplaceholder.typicode.com/posts', {
// //         title: 'foo',
// //         body: 'bar',
// //         userId: 1,
// //       })
// //       .then(function (response) {
// //         // handle success
// //         alert(JSON.stringify(response.data));
// //       })
// //       .catch(function (error) {
// //         // handle error
// //         alert(error.message);
// //       });
// //   };

// //   const multipleRequestsInSingleCall = () => {
// //     axios
// //       .all([
// //         axios
// //           .get('https://jsonplaceholder.typicode.com/posts/1')
// //           .then(function (response) {
// //             // handle success
// //             alert('Post 1 : ' + JSON.stringify(response.data));
// //           }),
// //         axios
// //           .get('https://jsonplaceholder.typicode.com/posts/2')
// //           .then(function (response) {
// //             // handle success
// //             alert('Post 2 : ' + JSON.stringify(response.data));
// //           }),
// //       ])
// //       .then(
// //         axios.spread(function (acct, perms) {
// //           // Both requests are now complete
// //           alert('Both requests are now complete');
// //         }),
// //       );
// //   };

// //   return (
// //     <View style={styles.container}>
// //       <Text style={{fontSize: 30, textAlign: 'center'}}>
// //         Example of Axios Networking in React Native
// //       </Text>
// //       {/*Running GET Request*/}
// //       <TouchableOpacity
// //         style={styles.buttonStyle}
// //         onPress={getDataUsingSimpleGetCall}>
// //         <Text>Simple Get Call</Text>
// //       </TouchableOpacity>

// //       <TouchableOpacity
// //         style={styles.buttonStyle}
// //         onPress={getDataUsingAsyncAwaitGetCall}>
// //         <Text>Get Data Using Async Await GET</Text>
// //       </TouchableOpacity>

// //       <TouchableOpacity
// //         style={styles.buttonStyle}
// //         onPress={postDataUsingSimplePostCall}>
// //         <Text>Post Data Using POST</Text>
// //       </TouchableOpacity>

// //       <TouchableOpacity
// //         style={styles.buttonStyle}
// //         onPress={multipleRequestsInSingleCall}>
// //         <Text>Multiple Concurrent Requests In Single Call</Text>
// //       </TouchableOpacity>

// //       <Text style={{textAlign: 'center', marginTop: 18}}>
// //         www.aboutreact.com
// //       </Text>
// //     </View>
// //   );
// // };

// // const styles = StyleSheet.create({
// //   container: {
// //     justifyContent: 'center',
// //     flex: 1,
// //     padding: 16,
// //   },
// //   buttonStyle: {
// //     alignItems: 'center',
// //     backgroundColor: '#DDDDDD',
// //     padding: 10,
// //     width: '100%',
// //     marginTop: 16,
// //   },
// // });

// // export default App;