import React,{Component} from "react";
import {NavigationContainer} from "@react-navigation/native"
import {createStackNavigator} from "@react-navigation/stack"

const stack=createStackNavigator()
const MyStack=()=>{
   <NavigationContainer>
       <Stack.Navigator>
           <Stack.Screen
           name="Login"
           component={Login}
           options={{title:"Login"}}
           />
           <Stack.Screen name="Signup" component="Signup" options={{title:"Signup"}}/>
       </Stack.Navigator>
   </NavigationContainer>
}

export default MyStack;