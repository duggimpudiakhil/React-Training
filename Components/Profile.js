import React,{ Component } from 'react';
import { View ,Text, Image,TouchableOpacity} from 'react-native';
import { connect } from 'react-redux';

class Profile extends Component{
    constructor(props){
        super(props)
        this.textInput=React.createRef();
        this.fall = new Animated.Value(1);
    }
    launchCamera = () => {
        ImagePicker.openCamera({
          width: 300,
          height: 400,
          cropping: true,
        }).then(image => {
          this.setState({
            photo: image.path
          })
          this.textInput.current.snapTo(1)
        }).catch(error => {
          console.log(error)
        });
    
      }
    
      launchImageLibrary = () => {
    
        ImagePicker.openPicker({
          width: 300,
          height: 400,
          cropping: true
        }).then(image => {
          this.setState({ photo: image.path })
          console.log(image.path);
          this.textInput.current.snapTo(1);
        }).catch(err => {
          console.log(err)
        });
    
      }
      listItems = () => (
    
    
        <View style={styles.btnParentSection}>
          <View style={styles.panelHeading}>
            <Text style={styles.panelBtnText}>Upload Photo</Text>
            <Text style={{ fontSize: 15 }}>Choose from photos</Text>
          </View>
          <TouchableOpacity onPress={this.launchCamera} style={styles.btnSection}  >
            <Text style={styles.btnText}>Directly Launch Camera</Text>
          </TouchableOpacity>
    
          <TouchableOpacity onPress={this.launchImageLibrary} style={styles.btnSection}  >
            <Text style={styles.btnText}>Directly Launch Image Library</Text>
          </TouchableOpacity>
          <TouchableOpacity onPress={() => this.textInput.current.snapTo(1)} style={styles.btnSection}  >
            <Text style={styles.btnText}>Cancel</Text>
          </TouchableOpacity>
    
        </View>
      );
    render(){
    <View>
        <BottomSheet
          ref={this.textInput}
          snapPoints={[400, 0]}
          renderContent={this.listItems}
          renderHeader={this.renderHeader}
          initialSnap={1}
          callbackNode={this.fall}
          enabledGestureInteraction={true}
        />
        <View>
           <TouchableOpacity onPress={() => this.textInput.current.snapTo(0)}>
             
             <Image
               source={{ uri: this.state.photo }}
               style={styles.imageStyle}
             />
            </TouchableOpacity>
        </View>
    </View>
}
}

function mapStateToProps(state){
    return{
      photo:state.login.photo
    }
}
export default connect(mapStateToProps)(Profile)