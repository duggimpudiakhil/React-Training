
import React from 'react'
import MapView from 'react-native-maps'; // remove PROVIDER_GOOGLE import if not using Google Maps
import {StyleSheet,View} from 'react-native'
const styles = StyleSheet.create({
 container: {
  
   height: 400,
   width: 400,
   justifyContent: 'flex-end',
   alignItems: 'center',
 },
 map: {
    ...StyleSheet.absoluteFillObject,
 },
});

export default Maps=() => (
   <View style={styles.container}>
     <MapView
      // provider={PROVIDER_GOOGLE} // remove if not using Google Maps
        style={styles.map}

         initialRegion={{
         latitude: 37.78825,
         longitude: -122.4324,
         latitudeDelta: 0.0922,
         longitudeDelta: 0.0421,
         }}

       showsUserLocation={true}  
      />
    
   </View>
);