import React from "react";
import { View, Text, FlatList, StyleSheet, ActivityIndicator } from 'react-native'

import { fetchData,getData } from '../Redux/actions/apiAction'
import { connect } from 'react-redux'
import { TouchableOpacity } from "react-native-gesture-handler";

class ApiData extends React.Component {
    componentDidMount() {
        this.props.doChange();
    }

    renderItem({ item }) {


        return (
            <View style={styles.container}>
                <TouchableOpacity
                    
                    onPress={() =>{
                        this.props.getApiData(item)
                        this.props.navigation.navigate('Description')}
                    }
                >
                    <Text style={styles.text}>API: {item.API}</Text>
                    <Text style={styles.text}>Category: {item.Category}</Text>
                </TouchableOpacity>
            </View>
        );
    }

    render() {
        
        return (
            <View>
                {
                    
                    this.props.isLoading ?
                        <View>
                            <ActivityIndicator size="large" color="red" />
                        </View>
                        :
                        <View>
                            <FlatList
                                data={this.props.datalist.entries}
                                keyExtractor={(item) => item.Link}
                                renderItem={(item) => this.renderItem(item)}
                            />
                        </View>
                }
            </View>
        )
    }
}
function mapStateToProps(state) {
    return {
        datalist: state.api.datalist,
        isLoading: state.api.isLoading,
    }
}
function mapDispatchToProps(dispatch) {
    return {
        doChange: () => { dispatch(fetchData()) },
        getApiData:(item)=>{dispatch(getData(item))}
    }
}

const styles = StyleSheet.create({
    container: {
        flex: 1,
        marginHorizontal: 13,
        marginVertical: 13,
        height: 80,
        backgroundColor: 'silver'
    },
    text: {
        fontSize: 25,
    }
})
export default connect(mapStateToProps, mapDispatchToProps)(ApiData);
