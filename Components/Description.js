import React,{Component} from 'react'
import { View ,Text, StyleSheet, Linking, TouchableOpacity} from 'react-native'
import { connect } from 'react-redux'

class Description extends Component{
    render(){
        
       // const data=this.props.route.params.item
       
        return(
           <View>
               <View style={styles.container}>
                   <Text style={styles.sideheading}>API</Text>
                  <Text style={styles.text}> :  {this.props.data.API}</Text>
               </View>
               <View style={styles.container}>
                  <Text style={styles.sideheading}>Description</Text>
                  <Text style={styles.text}> :  {this.props.data.Description}</Text>
               </View>
               <View style={styles.container}>
                  <Text style={styles.sideheading}>HTTPS</Text>
                  <Text style={styles.text}> : {this.props.data.HTTPS.toString()}</Text>
               </View>
               <View style={styles.container}>
                 <Text style={styles.sideheading}>Cors</Text>
                 <Text style={styles.text}> : {this.props.data.Cors}</Text>
               </View>
               <View style={styles.container}>
                  <Text style={styles.sideheading}>Link</Text>
                  <Text style={{fontSize:20}}> : </Text>
                   <TouchableOpacity
                   style={{}}
                         onPress={()=>Linking.openURL(this.props.data.Link)}
                   >  
                        <Text style={styles.link}>{this.props.data.Link}</Text>
                   </TouchableOpacity>
               </View>
               <View style={styles.container}>
                    <Text style={styles.sideheading}>Category</Text>
                    <Text style={styles.text}> : {this.props.data.Category}</Text>
               </View>
               
               
               
           </View>
        )
    }
}

function mapStateToProps(state){
   console.log(state.api.apidata) 
    return{
        data:state.api.apidata,
    }
}

const styles=StyleSheet.create({
    container:{
     flexDirection:"row",
     
    },
    sideheading:{
        fontSize:22,
        color:"green",
        textDecorationLine:"underline",
    },
    text:{
        fontSize:22, 
       
    },
    link:{
        fontSize:23,
        textDecorationLine:"underline",
        color:"blue"
    }
})

export default connect(mapStateToProps)(Description)