import React,{Component} from "react"
import {View,Button,Text, StyleSheet,Image} from "react-native"
import {connect} from 'react-redux';


class Details extends Component{
  render(){ 
    // const x=this.props.route.params
    const data=this.props.detailsData;
    console.log(this.props.detailsData)
    return (
     <View  style={{}} > 
      
       <Image
       source={{uri:data.photo}}
       style={styles.image}
       />
      <Text style={{fontSize:40,color:"red"}}>Description:  </Text>

      <Text style={{fontSize:25}}>Name: {data.name}</Text>
      <Text style={{fontSize:25}}>Email:  {data.email}</Text>
      <Text style={{fontSize:25}}>Position: {data.position}</Text>
      {/* {/* <Button
        title="Go to Home"
        onPress={() =>
         
        this.props.navigation.navigate('Login')
        } */}
      </View>
    );
  }
  }
  function mapStateToProps(state){
    return{
      detailsData: state.employee.detailsData
    }
  }


  const styles=StyleSheet.create({
    image:{
      width:"100%",
      height:"50%",
      borderWidth:2,
      borderColor:'silver',
      resizeMode:"stretch"
     
    },
  })
  export default connect(mapStateToProps)(Details)