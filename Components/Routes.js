import "react-native-gesture-handler"

import React,{Component} from "react";
import { createDrawerNavigator } from '@react-navigation/drawer';
import {NavigationContainer} from "@react-navigation/native"
import {createStackNavigator} from "@react-navigation/stack"

import HomeActivity from './HomeActivity'
import GridMenu from './Grid'
import FoodItems from './FoodItems'
import Maps from './Maps'
import Details from './Details'

import ApiData from './ApiData'
import EmployeeDetails from "./EmployeeDetails";
import Description from './Description'

import  Icon  from "react-native-vector-icons/Ionicons";
//import {Signup} from "./Signup"
const HomeStack=createStackNavigator();
const GridStack=createStackNavigator();
const FoodStack=createStackNavigator();
const EmployeeStack=createStackNavigator();
const DetailsStack=createStackNavigator();
const APIStack=createStackNavigator();
const DescriptionStack=createStackNavigator();
const MapStack=createStackNavigator();

const Drawer=createDrawerNavigator()

const HomeStackScreen=({navigation})=>(
   <HomeStack.Navigator>
      <HomeStack.Screen
             name="Login"
             component={HomeActivity}
             options={{title:"Login Page",headerStyle:{
                backgroundColor:"#009387"
             },headerTitleAlign:"center",
             headerLeft:()=>(
               <Icon.Button name="ios-menu" size={25} backgroundColor="#009387"
               onPress={()=>navigation.openDrawer()}
               ></Icon.Button> 
            )  }}
      />
   </HomeStack.Navigator>
);
const MapStackScreen=({navigation})=>(
   <MapStack.Navigator>
      <MapStack.Screen
        name="Maps"
        component={Maps}
        options={{title:"Maps",headerTitleAlign:"center",
        headerStyle:{
             backgroundColor:"#009387"
        },
        headerLeft:()=>{
           <Icon.Button name="ios-menu" size={25} backgroundColor="#009387"
           onPress={()=>navigation.toggleDrawer()}
           ></Icon.Button>
        }
      }}
      ></MapStack.Screen>
   </MapStack.Navigator>
)
const GridStackScreen=({navigation})=>(
   <GridStack.Navigator>
      <GridStack.Screen
             name="Grid"
             component={GridMenu}
             options={{title:"Grid Menu",headerStyle:{
               backgroundColor:"#009387"},headerTitleAlign:"center",
               headerLeft:()=>(
                  <Icon.Button name="ios-menu" size={25} backgroundColor="#009387"
                  onPress={()=>navigation.openDrawer()}
                  ></Icon.Button> 
               )  }}
      />
   </GridStack.Navigator>
);

const FoodStackScreen=({navigation})=>(
   <FoodStack.Navigator>
      <FoodStack.Screen
             name="Food"
             component={FoodItems}
             options={{title:"Food Items",headerStyle:{
               backgroundColor:"#009387"},headerTitleAlign:"center",
               headerLeft:()=>(
                  <Icon.Button name="ios-menu" size={25} backgroundColor="#009387"
                  onPress={()=>navigation.openDrawer()}
                  ></Icon.Button> 
               )  }}
      />
   </FoodStack.Navigator>
);
const EmployeeStackScreen=({navigation})=>(
   <EmployeeStack.Navigator>
      <EmployeeStack.Screen
             name="Employees"
             component={EmployeeDetails}
             options={{title:"Employees of AK Organisation",headerStyle:{
               backgroundColor:"#009387"},headerTitleAlign:"center",
               headerLeft:()=>(
                  <Icon.Button name="ios-menu" size={25} backgroundColor="#009387"
                  onPress={()=>navigation.openDrawer()}
                  ></Icon.Button> 
               )  }}
      />
   </EmployeeStack.Navigator>
);
const DetailsStackScreen=({navigation})=>(
   <DetailsStack.Navigator>
      <DetailsStack.Screen
             name="Details"
             component={Details}
             options={{title:"Details",headerStyle:{
               backgroundColor:"#009387"},headerTitleAlign:"center",
               headerLeft:()=>(
                  <Icon.Button name="ios-menu" size={25} backgroundColor="#009387"
                  onPress={()=>navigation.openDrawer()}
                  ></Icon.Button> 
               )  }}
      />
   </DetailsStack.Navigator>
);
const APIStackScreen=({navigation})=>(
   <APIStack.Navigator>
      <APIStack.Screen
             name="ApiData"
             component={ApiData}
             options={{title:"APIData",headerStyle:{
               backgroundColor:"#009387"},headerTitleAlign:"center",
               headerLeft:()=>(
                  <Icon.Button name="ios-menu" size={25} backgroundColor="#009387"
                  onPress={()=>navigation.toggleDrawer()}
                  ></Icon.Button> 
               )  }}
      />
   </APIStack.Navigator>
);
const DescriptionStackScreen=({navigation})=>(
   <DescriptionStack.Navigator>
      <DescriptionStack.Screen
             name="Description"
             component={Description}
             options={{title:"Description",
             headerStyle:{
               backgroundColor:"#009387"
            },
            headerTitleAlign:"center",
            headerLeft:()=>(
               <Icon.Button name="ios-menu" size={25} backgroundColor="#009387"
               onPress={()=>navigation.openDrawer()}
               ></Icon.Button> 
            )  
         }
            }
            
      />
   </DescriptionStack.Navigator>
);
export  const MyStack=()=> {
 
   return(
      <NavigationContainer>
         <Drawer.Navigator>
            <Drawer.Screen name="Login" component={HomeStackScreen} />
            <Drawer.Screen name="Grid" component={GridStackScreen} />
            <Drawer.Screen name="Food" component={FoodStackScreen} />
            <Drawer.Screen name="Employees" component={EmployeeStackScreen} />
            <Drawer.Screen name="Maps" component={MapStackScreen} />
            <Drawer.Screen name="Details" component={DetailsStackScreen} />
            <Drawer.Screen name="ApiData" component={APIStackScreen} />
            <Drawer.Screen name="Description" component={DescriptionStackScreen} />
         </Drawer.Navigator>
         {/* <Stack.Navigator>
             <Stack.Screen
             name="Login"
             component={HomeActivity}
             options={{title:"Login Page",headerTitleAlign:"center"}}
             />
             <Stack.Screen name="Grid" component={GridMenu} options={{title:"Menu"}}/>
             <Stack.Screen name="Food" component={FoodItems} options={{title:"Food List"}}/>
             <Stack.Screen name="Employees" component={EmployeeDetails} options={{title:"Employees of AK Organisation"}}/>
             <Stack.Screen name="Details" component={Details} options={{title:"Details"}}/>
             <Stack.Screen name="ApiData" component={ApiData} options={{title:"APIData"}}/>
             <Stack.Screen name="Description" component={Description} options={{title:"Description"}}/>
         </Stack.Navigator> */}

      </NavigationContainer>
     );
   
   }


