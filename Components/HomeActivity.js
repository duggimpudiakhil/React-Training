import React, { Component } from "react"
import { View, Image, Text, TextInput, TouchableNativeFeedback, Alert, StyleSheet, ImageBackground } from "react-native"
//import { State } from "react-native-gesture-handler";
import { connect } from "react-redux";
//import { RNCamera } from 'react-native-camera';
import Icon from 'react-native-vector-icons/MaterialIcons';
import ImagePicker from 'react-native-image-crop-picker'
import BottomSheet from 'reanimated-bottom-sheet'
import Animated from 'react-native-reanimated';
import { changeUname, changePwd, reset,changePhoto } from '../Redux/actions/loginaction'
import {  TouchableOpacity } from "react-native-gesture-handler";

class HomeActivity extends Component {
  constructor(props) {
    super(props)
    
    this.textInput = React.createRef();
    this.fall = new Animated.Value(1);
  }

  launchCamera = () => {
    ImagePicker.openCamera({
      width: 300,
      height: 400,
      cropping: true,
    }).then(image => {
      var pic=image.path;
      console.log(pic)
      this.props.changePic(pic)
      this.textInput.current.snapTo(1)
    }).catch(error => {
      console.log(error)
    });

  }

  launchImageLibrary = () => {

    ImagePicker.openPicker({
      width: 300,
      height: 400,
      cropping: true
    }).then(image => {
      //this.setState({ photo: image.path })
      var pic=image.path;
      this.props.changePic(pic);
     

      this.textInput.current.snapTo(1);
    }).catch(err => {
      console.log(err)
    });

  }
  listItems = () => (

    <View style={styles.btnParentSection}>
      <View style={styles.panelHeading}>
        <Text style={styles.panelBtnText}>Upload Photo</Text>
        <Text style={{ fontSize: 15 }}>Choose from photos</Text>
      </View>
      <TouchableOpacity onPress={this.launchCamera} style={styles.btnSection}  >
        <Text style={styles.btnText}>Directly Launch Camera</Text>
      </TouchableOpacity>

      <TouchableOpacity onPress={this.launchImageLibrary} style={styles.btnSection}  >
        <Text style={styles.btnText}>Directly Launch Image Library</Text>
      </TouchableOpacity>
      <TouchableOpacity onPress={() => this.textInput.current.snapTo(1)} style={styles.btnSection}  >
        <Text style={styles.btnText}>Cancel</Text>
      </TouchableOpacity>

    </View>
  );

  render() {
    return (

      <View style={styles.container}>
        <BottomSheet
          ref={this.textInput}
          snapPoints={[400, 0]}
          renderContent={this.listItems}
          renderHeader={this.renderHeader}
          initialSnap={1}
          callbackNode={this.fall}
          enabledGestureInteraction={true}
        />
        <Animated.View style={{ opacity: Animated.add(0.1, Animated.multiply(this.fall, 1.0)) }}>
          <View style={styles.formContainer}>

            <TouchableOpacity onPress={() => this.textInput.current.snapTo(0)}>
             
              <Image
                source={{ uri: this.props.photo }}
                style={styles.imageStyle}
              />
              
            </TouchableOpacity>
            <TouchableOpacity>
               <Text style={styles.formText}>Not a User? Register</Text>
            </TouchableOpacity>

          </View>

          <View>

            <Text style={styles.header}>Username:</Text>

            <TextInput
              style={styles.text}
              placeholder="Username"

              value={this.props.uname}
              //autoFocus={true}
              onChangeText={(text) => this.props.doChangeU(text)}

            >

            </TextInput>

            <Text style={styles.header}>Password:</Text>
            <TextInput
              style={styles.text}
              placeholder="Password"
              secureTextEntry={true}
              value={this.props.pwd}
              onChangeText={(text) => this.props.doChange(text)}

            />

          </View>
          <View style={styles.btnContainer}>

            <TouchableNativeFeedback

              onPress={() => {
                console.log(this.props.photo)
                if (this.props.uname == "Admin" && this.props.pwd == "Admin") {

                  this.props.navigation.navigate('Grid')

                }
                else {

                  Alert.alert("Invalid Credentials")
                }
              }
              }
            >
              <View style={styles.button}>
                <Text style={{ fontSize: 25 }}>Login</Text>
              </View>

            </TouchableNativeFeedback>
            <TouchableNativeFeedback
              style={styles.button}
              title="Reset"
              onPress={() => this.props.doReset()}
            >
              <View style={styles.button}>
                <Text style={{ fontSize: 25 }}>Reset</Text>
              </View>
            </TouchableNativeFeedback>

          </View>
        </Animated.View>
      </View>

    );
  }
}
function mapStateToProps(state) {
  
  return {
    uname: state.login.uname,
    pwd: state.login.pwd,
    photo: state.login.photo,
  }
}
function mapDispatchToProps(dispatch) {
  return {

    doChangeU: (text) => { dispatch(changeUname(text)) },
    doChange: (val) => { dispatch(changePwd(val)) },
    doReset: () => { dispatch(reset()) },
    changePic:(pic)=>{dispatch(changePhoto(pic))}
  }
}

const styles = StyleSheet.create({
  container: {
    flex: 1,
    backgroundColor: "silver",

  },
  formContainer: {
    flexDirection: "row",
    alignItems:"center",
    paddingVertical:10,
    
  },
  imageStyle: {
    width: 110,
    height: 110,
    borderWidth: 2,
    borderColor: 'grey',
    overflow: 'hidden',
    borderRadius: 50
  },
  formText:{
     fontSize:20,
     fontFamily:"AntDesign",
     marginLeft:20,
     justifyContent:"center"
  },
  btnContainer: {

    top: 20,
    height: 50,
    flexDirection: "row",
    justifyContent: "space-around",

  },
  header: {
    marginTop: 10,
    marginLeft: 10,
    marginRight: 70,
    fontSize: 25
  },

  text: {
    fontSize: 20,
    marginLeft: 10,
    marginRight: 70,
    backgroundColor: "white",
    borderWidth: 2,
    borderColor: "green",
    borderRadius: 20
    // marginLeft: 40,
    // marginRight: 40,


  },
  button: {
    paddingLeft: 10,
    paddingHorizontal: 10,
    fontSize: 30,
    backgroundColor: "green"

  },
  btnParentSection: {
    backgroundColor: "#FFFDD0",
    alignItems: "center",
    justifyContent: "center",
    height: 400,
    width: "100%",
    padding: 20,

  },
  btnSection: {
    width: 350,
    paddingHorizontal: 15,
    paddingVertical: 15,
    borderWidth: 2,
    borderRadius: 30,

    marginVertical: 10,
    backgroundColor: "orange"
  },
  btnText: {
    fontSize: 15,

    alignContent: "center"
  },
  panelBtnText: {
    fontSize: 30,
    fontFamily:"FontAwesome5_Brands"
  },
  panelHeading: {
    padding: 20,
    alignItems: "center"
  }
})
export default connect(mapStateToProps, mapDispatchToProps)(HomeActivity)


