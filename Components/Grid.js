import React,{Component} from "react"
import {View,Button,Text,StyleSheet,TouchableOpacity, FlatList} from "react-native"

import {connect} from 'react-redux';

class GridMenu extends Component{
constructor(props){
  super(props)
  this.state={
    datalist:[{
     "key":"1",
     "name":"Food"
    },
    {
      "key":"2",
      "name":"Employees"
    },
    {
      "key":"3",
      "name":"ApiData"
    }
    
  ]
  }
}
  
renderItem({item}){
  return(
  <View style={styles.container}>
    <TouchableOpacity onPress={()=>{
      this.props.navigation.navigate(item.name)}}>
    
    <Text style={{height:130,top:25,fontSize:29}}>{item.name}</Text>

    </TouchableOpacity>

  </View>
  );
}

  render(){ 
  return (
    <View>
      
      
      <FlatList
       
      numColumns={2}
      // style={{justifyContent:"space-around", marginHorizontal:20,marginVertical:20,marginLeft:20,marginRight:20,backgroundColor:"red"}}
       data={this.state.datalist}
       renderItem={(item)=>this.renderItem(item)}
      />
      
      

    </View>
    
    );
  }
  }

function mapStateToProps(state){
  //  console.log(state.uname)  
    return {
        
        uname :state.uname,
        pwd:state.pwd
      }
  }
  export default connect(mapStateToProps)(GridMenu)

  const styles=StyleSheet.create({
    container:{
        flex:1,
        // flexDirection:"row",
        alignItems:"center",
        justifyContent:"center",
        
       marginHorizontal:20,
        marginVertical:20,
        
        borderWidth:4,
        borderColor:"black",
        backgroundColor:"silver"
    }, 
    

 })