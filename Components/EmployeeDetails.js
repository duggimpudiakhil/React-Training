import React, { Component } from "react"
import { View, Button, Text, FlatList, Image, StyleSheet, TouchableOpacity } from "react-native"
import { connect } from 'react-redux';
import {empDetails} from '../Redux/actions/employeeAction'
class EmployeeDetails extends Component {

    renderItem({ item }) {
        return (
            <>
                <View style={styles.container}>
                    <TouchableOpacity
                        onPress={() => {
                            this.props.employee(item);
                            this.props.navigation.navigate('Details')
                        }}
                    >
                        <Image source={{ uri: item.photo }}
                            style={styles.image}
                        />
                    </TouchableOpacity>

                    <View style={{ flex: 0.8, justifyContent: "center", alignItems: "center" }}>
                        <Text style={{ fontWeight: "bold", fontSize: 20, }}>{item.name}</Text>
                        <Text style={{ fontSize: 20 }}>{item.position}</Text>
                    </View>


                </View>

            </>
        );
    }
    render() {
        return (
            <View style={{}}>
                <FlatList

                    keyExtractor={(item) => item.email}
                    data={this.props.datalist}
                    renderItem={(item) => this.renderItem(item)}
                />
            </View>
        );
    }
}
function mapStateToProps(state) {
    return {
        datalist: state.employee.employeeData
    }
}
function mapDispatchToProps(dispatch) {
   return{
   employee:(item)=>{dispatch(empDetails(item))}
   }
}
const styles = StyleSheet.create({
    container: {
        flex: 1,
        flexDirection: "row",

        marginHorizontal: 13,
        marginVertical: 13,
        marginLeft: 20,
        marginRight: 20,
        borderColor: "yellow",
        backgroundColor: "silver"
    },
    image: {
        width: 110,
        height: 110,
        borderWidth: 2,
        borderColor: 'green',
        borderRadius: 50
    },

})
export default connect(mapStateToProps,mapDispatchToProps)(EmployeeDetails)