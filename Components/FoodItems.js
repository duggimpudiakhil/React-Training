import React, { Component } from "react"
import { View, Button, Text, FlatList, StyleSheet, TouchableOpacity, Image, ImageBackground } from "react-native"
import { connect } from "react-redux";
import {foodDetails} from "../Redux/actions/foodAction"
class FoodItems extends Component {

  renderItem({ item }) {

    return (
      <>
        <View style={styles.container}>
          <TouchableOpacity
            onPress={() => {

              this.props.food(item)
              this.props.navigation.navigate('Details')
            }}
          >
            <ImageBackground source={{ uri: item.photo }}
              style={styles.image}
            />
          </TouchableOpacity>

          <View style={{ flex: 0.8, justifyContent: "center", alignItems: "center" }}>
            <Text style={{ fontWeight: "bold", fontSize: 20, }}>{item.name}</Text>
            {/* <Text style={{fontSize:20}}>{item.position}</Text> */}
          </View>


        </View>

      </>
    );
  }
  render() {
    return (
      <View>
        <FlatList
          data={this.props.foodData}
          keyExtractor={(item) => item.email}
          renderItem={(item) => this.renderItem(item)}

        />

      </View>
    );
  }
}
function mapStateToProps(state){
 return {
   foodData: state.employee.foodData
  }
}
function mapDispatchToProps(dispatch){
  return{
   food:(item)=>{ dispatch(foodDetails(item)) }
  }
}
const styles = StyleSheet.create({
  container: {
    flex: 1,


    marginHorizontal: 13,
    marginVertical: 13,
    marginLeft: 20,
    marginRight: 20,
    borderColor: "yellow",
    backgroundColor: "silver"
  },
  image: {
    width: 370,
    height: 150,


  },

})
export default connect(mapStateToProps,mapDispatchToProps)(FoodItems)